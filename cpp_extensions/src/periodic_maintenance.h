#define _USE_MATH_DEFINES

#include "utils.h"


namespace PERIODIC_MAINTENANCE
{
struct ADJ
//information about an adjacent vertex in a predecessor/successor list
{
	int To;
	double Length;
};

struct STRATEGY
//structure describing a strategy (note that some edges
//of the graph may be unused by the strategy)
{
	ARRAY2D<double, MAXQ, MAXQ> Prob; //Prob[i][j] is the probability of going to state j when being at state i
	ARRAY1D<int, MAXQ> OutDegree; //OutDegree[i] is the number of actual successors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Succ; //Succ[i][j] is the j-th actual successor of state i
	ARRAY1D<int, MAXQ> InDegree; //InDegree[i] is the number of actual predecessors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> Pred; //Pred[i][j] is the j-th actual predecessor of state i

	ARRAY1D<int, MAXQ> IsReachable; //whether each state is reachable
};


struct GRAPH
{
	struct PAR_MANAGER_CALLER
	{
		PAR_MANAGER_CALLER(GRAPH* G,
		                   const std::vector<int>& memory,
		                   int g)
		{
			ParManager.Init(G, sizeof(GRAPH), memory, g);
		}
	}
	PMC;

	int n; //number of vertices
	int q; //number of states
	int g; //number of targets
	int qg; //number of states corresponding to targets
	ARRAY1D<std::vector<double>, MAXG> Time; //Time[i][j] is the right boundary of the j-th interval for revisiting target i,
	        //on top of the input from pyzbleft, a sentinel INFTY is added at the end
	ARRAY1D<double, MAXG> MaxTime; //MaxTime[i] is the last (non-sentinel) boundary among Time[i][...]
	ARRAY1D<std::vector<double>, MAXG> Cost; //Cost[i][j] is the payoff for revisiting target i in the j-th interval
	        //the last-but-one value is LastCost (see below), qqq
	        //the last value is the Slope (see below)
	        //note that Cost[i].size() is Time[i].size() + 2
	ARRAY1D<double, MAXG> LastCost; //LastCost[i] is the cost for the last (the only infinite) interval in the constantized function
	ARRAY1D<double, MAXG> Slope; //Slope[i] is the derivative of the value in the last interval
	ARRAY1D<int, MAXN> MemorySize; //MemorySize[i] is the memory size of vertex i

#define INFTY ((double) (1ULL << 63))
	ARRAY2D<double, MAXN, MAXN> VEdgeLength; //INFTY = no edge

	/*double MaxWeight; //maximum of Weight[i] over all targets i
	double OrigMaxWeight; //in case the above gets normalized to 1.
	int tMaxWeight; //index of the most valued target*/

	ARRAY2D<double, MAXN, MAXN> VDist; //for Floyd-Warshall


	ARRAY1D<int, MAXN> VInDegree; //VInDegree[i] is the number of predecessors of vertex i
	ARRAY2D<ADJ, MAXN, MAXN> VPred; //VPred[i][j] is the j-th predecessor of vertex i
	ARRAY1D<int, MAXN> VOutDegree; //VOutDegree[i] is the number of successors of vertex i
	ARRAY2D<ADJ, MAXN, MAXN> VSucc; //VSucc[i][j] is the j-th successor of vertex i

	ARRAY1D<int, MAXQ> Q2V; //Q2V[i] is the vertex to which state i corresponds
	ARRAY1D<int, MAXN> V2Q; //V2Q[i] is the first state which corresponds to vertex i
	ARRAY1D<int, MAXQ> GlobOutDegree; //GlobOutDegree[i] is the number of successors of state i
	ARRAY2D<ADJ, MAXQ, MAXQ> GlobSucc; //GlobSucc[i][j] is the j-th successor of state i

	/*ARRAY1D<int, MAXQ> Stamp; //the time stamp for cycle search
	ARRAY1D<int, MAXQ> Q; //queue for reachability BFS
	int QBeg, QEnd;*/

	void Unfold(int i, int& v, int& e);
	void InitVSucc(bool Check);
	void InitFM();
	void FloydWarshall(double bonus);

	void InitStrategyAdj();

	GRAPH(std::vector<int> memory,
	      std::vector<std::vector<double>> time,
	      std::vector<std::vector<double>> cost,
	      std::vector<EDGE_TYPE> edge):
	        PMC(this, memory, time.size())
	{
		ParManager.InitFinished();
		this->Init(memory, time, cost, edge);
	}

	void Init(const std::vector<int>& memory,
	          const std::vector<std::vector<double>>& time,
	          const std::vector<std::vector<double>>& cost,
	          const std::vector<EDGE_TYPE>& edge);

	void LoadStrategy(at::Tensor /*double[1][Q][Q]*/ strat);

	at::Tensor /*double[QG]*/ PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat, double sigma);
	at::Tensor /*double[Q][Q]*/ PyComputeGradient(at::Tensor /*double[QG]*/ imps);

	std::pair<std::vector<std::pair<int, double>>, std::vector<double>>
	PyDeterminize(at::Tensor /*double[Q][Q]*/ strat, int sampleLength, int maxRslLength, int seed, int initState);

	int GetRandomSuccessor(int i);
	double GetLoopValue(const std::vector<std::pair<int, double>>& path, int beg, int end);
	ARRAY1D<double, MAXN> FirstVisit;
	ARRAY1D<double, MAXN> LastVisit;

	STRATEGY S;
	double sigma;
	double pmt;

	struct HEAPITEM
	{
		double d; //distance towards the examined target
		double p; //probability of the corresponding path(s)
		double cost; //the cost for this item
		double der; //the derivative for this item
		int i; //initial state
		int id; //id of this heap-item

		bool operator < (const HEAPITEM& other) const
		{
			return this->d > other.d;
		}
	};

	std::vector<HEAPITEM> Heap;
	ARRAY1D<std::vector<HEAPITEM>, MAXG> HeapList;
	ARRAY1D<std::vector<int>, MAXG> LabelO2N;
	ARRAY1D<int, MAXG> nLabels;
	ARRAY1D<std::vector<std::pair<int, int> >, MAXG> Contrib;
	std::vector<double> PD; //PD[i] is the partial derivative of the examined
	    //loss function with respect to HeapList[t][i].p for the currently
	    //examined target t

	ARRAY1D<double, MAXQG> Payoff; //Payoff[i] is the expected payoff for the path from state i to the corresponding target

	ARRAY1D<std::pair<int, double>, MAXQ> CurListIndex;

	void ComputeValues();
	void Search(int t);
	double GetExactCost(int t, double x);
	void GetCostAndDer(int t, double x, double sigma, double& cost, double& der);

	std::vector<double> TotalPDForEdge; //the index is the edge
	void ComputeGradient(int t);
	ARRAY1D<double, MAXQG> ImpArray; //ImpArray[i] is the importance coefficient of the i-th state (which corresponds to a target)

	ARRAY2D<int, MAXQ, MAXQ> EdgeToIndex;
	std::vector<std::pair<int, int>> IndexToEdge;
};


void GRAPH::LoadStrategy(at::Tensor /*double[Q][Q]*/ strat)
{
	if(strat.dim() != 2)
	{
		THROW("LoadStrategy: q*q array expected, got " << strat.dim() << " dimension(s)!");
	}

	if((strat.size(0) != q) || (strat.size(1) != q))
	{
		THROW("LoadStrategy: q*q array expected for q = " << q << ", got " \
		      << strat.size(0) << "*" << strat.size(1) << "!");
	}

	double* p = strat.data_ptr<double>();
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			S.Prob[i][j] = *(p++);
		}
	}

	InitStrategyAdj();
	for(int i = 0; i < q; i++)
	//ignoring reachability in this objective
	{
		S.IsReachable[i] = 1;
	}
}


at::Tensor /*double[QG]*/ GRAPH::PyEvaluateStrategy(at::Tensor /*double[Q][Q]*/ strat, double sigma)
{
	LoadStrategy(strat);
	this->sigma = sigma;
	this->pmt = DEFAULT_PATH_MERGE_THRESHOLD;

	ComputeValues();

	at::Tensor rsl = torch::empty({qg}, torch::kFloat64);
	double* p = rsl.data_ptr<double>();
	for(int i = 0; i < qg; i++)
	{
		*(p++) = Payoff[i];
	}

	return rsl;
}


at::Tensor /*double[2][Q][Q]*/ GRAPH::PyComputeGradient(at::Tensor /*double[QG]*/ imps)
{
	if(imps.dim() != 1)
	{
		THROW("PyComputeGradient: 1 dimension expected, got " << imps.dim() << " dimension(s)!");
	}

	if(imps.size(0) != qg)
	{
		THROW("PyComputeGradient: expected size qg = " << qg << ", got " << imps.size(0) << "!");
	}

	double* impp = imps.data_ptr<double>();
	for(int i = 0; i < qg; i++)
	{
		ImpArray[i] = *(impp++);
	}


	IndexToEdge.clear();
	for(int a = 0; a < q; a++)
	{
		for(int b = 0; b < q; b++)
		{
			EdgeToIndex[a][b] = IndexToEdge.size();
			if(S.Prob[a][b] > 0.)
			{
				IndexToEdge.push_back({a, b});
			}
		}
	}

	int nEdges = IndexToEdge.size();


	at::Tensor rsl = torch::zeros({2, q, q}, torch::kFloat64);
	double* pProb = rsl.data_ptr<double>();

	for(int t = 0; t < g; t++)
	{
		for(int i = V2Q[t]; i < V2Q[t] + MemorySize[t]; i++)
		{
			if(ImpArray[i] != 0.)
			{
				goto MUST_COMPUTE_IT;
			}
		}

		continue;
        MUST_COMPUTE_IT:
		ComputeGradient(t);

		for(int e = 0; e < nEdges; e++)
		{
			auto& [a, b] = IndexToEdge[e];
			pProb[(a * q) + b] += TotalPDForEdge[e];
		}
	}

	return rsl;
}


std::pair<std::vector<std::pair<int, double>>, std::vector<double>>
GRAPH::PyDeterminize(at::Tensor /*double[Q][Q]*/ strat, int sampleLength, int maxRslLength, int seed, int initState)
{
	LoadStrategy(strat);

	if(seed)
	{
		srand(seed);
	}

	std::vector<std::pair<int, double>> path(sampleLength + 1);
	std::vector<std::pair<int, double>> bestPath;
	std::vector<double> vBestVal(sampleLength);
	double bestVal = -INFTY;

	path[0].first = initState;
	int curState = initState;

	int i = 0;
	while(i < sampleLength)
	{
		int succ = GetRandomSuccessor(curState);
		int nextState = S.Succ[curState][succ].To;
		path[i++].second = S.Succ[curState][succ].Length;
		path[i].first = nextState;
		curState = nextState;

		int beg;
		for(int j = 1; (j <= maxRslLength) && ((beg = i - j) >= 0); j++)
		{
			if(path[beg].first != curState)
			{
				continue;
			}

			double curVal = GetLoopValue(path, beg, i);
			if(curVal > bestVal)
			{
				bestVal = curVal;
				bestPath.clear();
				for(int k = 0; k < j; k++)
				{
					auto& p = path[beg + k];
					bestPath.push_back({Q2V[p.first], p.second});
				}
			}
		}

		vBestVal[i - 1] = bestVal;
	}

	return {bestPath, vBestVal};
}


void Minimize(double& a, double b);
void Maximize(double& a, double b);


int GRAPH::GetRandomSuccessor(int i)
{
	int rr = rand() % 16384;
	double r = rr / 16384.;
	double sum = 0.;

	for(int k = 0; 1; k++)
	{
		int j = S.Succ[i][k].To;
		sum += S.Prob[i][j];
		if((k == S.OutDegree[i] - 1) || (sum > r))
		{
			return k;
		}
	}
}


double GRAPH::GetLoopValue(const std::vector<std::pair<int, double>>& path, int beg, int end)
{
	for(int t = 0; t < g; t++)
	{
		FirstVisit[t] = -1.;
		LastVisit[t] = -1.;
	}

	double totalLength = 0.;
	double totalCost = 0.;
	for(int i = beg; i < end; i++)
	{
		int t = Q2V[path[i].first];
		if(t < g)
		//really a target
		{
			if(LastVisit[t] == -1.)
			{
				FirstVisit[t] = totalLength;
			}
			else
			{
				totalCost += GetExactCost(t, totalLength - LastVisit[t]);
			}

			LastVisit[t] = totalLength;
		}

		totalLength += path[i].second;
	}

	double totalSlope = 0.;
	for(int t = 0; t < g; t++)
	{
		totalSlope += Slope[t];
		if(FirstVisit[t] != -1.)
		{
			totalCost += GetExactCost(t, (totalLength + FirstVisit[t]) - LastVisit[t]);
		}
	}

	return (totalCost / totalLength) + totalSlope;
}


void GRAPH::FloydWarshall(double bonus)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			VDist[i][j] = VEdgeLength[i][j] - bonus;
		}

		Minimize(VDist[i][i], 0.);
	}

	for(int k = 0; k < n; k++)
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				Minimize(VDist[i][j], VDist[i][k] + VDist[k][j]);
			}
		}
	}
}


void GRAPH::Unfold(int i, int& v, int& e)
//unfolds state i to the corresponding vertex and memory element
{
	v = Q2V[i];
	int diff = i - V2Q[v];
	e = diff % MemorySize[v];
}


void GRAPH::InitVSucc(bool Check)
{
	for(int i = 0; i < n; i++)
	{
		VInDegree[i] = 0;
		VOutDegree[i] = 0;
	}

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double l = VEdgeLength[i][j];
			if(l < INFTY)
			{
				VPred[j][VInDegree[j]++] = (ADJ) {i, l};
				VSucc[i][VOutDegree[i]++] = (ADJ) {j, l};
			}
		}
	}

	if(Check)
	{
		for(int i = 0; i < n; i++)
		{
			assert(VOutDegree[i]);
		}
	}
}


void GRAPH::InitFM()
//initializes the arrays Q2V, V2Q, OutDegree, Succ
{
	q = 0;
	for(int i = 0; i < n; i++)
	{
		V2Q[i] = q;
		for(int j = 0; j < MemorySize[i]; j++)
		{
			Q2V[q++] = i;
		}
	}

//here you can hard-code the automaton part (or any restrictions to it)
	for(int i = 0; i < q; i++)
	{
		int v1, e1;
		Unfold(i, v1, e1);

		for(int j = 0; j < q; j++)
		{
			int v2, e2;
			Unfold(j, v2, e2);

			if(VEdgeLength[v1][v2] < INFTY)
			{
				GlobSucc[i][GlobOutDegree[i]++] = (ADJ) {j, VEdgeLength[v1][v2]};
			}
		}
	}
}


void GRAPH::InitStrategyAdj()
{
	for(int i = 0; i < q; i++)
	{
		S.OutDegree[i] = 0;
		S.InDegree[i] = 0;
	}

	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			if(S.Prob[i][j] > 0.)
			{
				S.Succ[i][S.OutDegree[i]++] = (ADJ) {j, VEdgeLength[Q2V[i]][Q2V[j]]};
			        S.Pred[j][S.InDegree[j]++]  = (ADJ) {i, VEdgeLength[Q2V[i]][Q2V[j]]};
	                }
                }
		}
}


void GRAPH::ComputeValues()
{
	/*long long TotalPV = 0;
	long long TotalPE = 0;*/

	for(int t = 0; t < g; t++)
	{
		Search(t);
		/*TotalPE += Contrib[t].size();
		TotalPV += HeapList[t].size();*/
	}
}


void GRAPH::Search(int t)
{
	double maxTime = MaxTime[t] + (10. * sigma);

	Heap.clear();
	HeapList[t].clear();

	LabelO2N[t].clear();
	nLabels[t] = 0;
	Contrib[t].clear();

	for(int i = 0; i < q; i++)
	{
		if(Q2V[i] == t)
		{
			Payoff[i] = LastCost[t]; //clearing (the default is to assume ending in the very last interval with probability 1)
			LabelO2N[t].push_back(nLabels[t]);
			Heap.push_back((HEAPITEM) {0., 1., 0., 0., i, nLabels[t]++});
		}
	}

	for(int i = 0; i < q; i++)
	{
		CurListIndex[i] = std::make_pair(-1, -INFTY);
	}

	while(!(Heap.empty()))
	{
		int iList = HeapList[t].size();
		double dLast;

		while(1)
		{
			HEAPITEM cur = Heap[0];
			dLast = cur.d;
			std::pop_heap(Heap.begin(), Heap.end());
			Heap.pop_back();

			if(cur.d <= CurListIndex[cur.i].second + pmt)
			{
				CurListIndex[cur.i].second = cur.d;
				LabelO2N[t][cur.id] = CurListIndex[cur.i].first;
				HeapList[t][CurListIndex[cur.i].first].d = cur.d;
				HeapList[t][CurListIndex[cur.i].first].p += cur.p;
			}
			else
			{
				CurListIndex[cur.i] = {HeapList[t].size(), cur.d};
				LabelO2N[t][cur.id] = HeapList[t].size();
				HeapList[t].push_back(cur);
			}

			if((Heap.empty()) || (Heap[0].d > cur.d + pmt))
			{
				break;
			}
		}

		for(; iList < (int) HeapList[t].size(); iList++)
		{
			HEAPITEM& cur = HeapList[t][iList];
			cur.d = dLast; //must rewrite it in the item because
			        //in which interval the path length (.d) of this node lies,
			        //is determined by this value when computing gradient
			int i = cur.i;

			if((cur.id >= MemorySize[t]) && (Q2V[i] == t))
			//the first condition excludes reaching the target in 0 steps
			{
				GetCostAndDer(t, cur.d, sigma, cur.cost, cur.der);
				Payoff[i] += cur.p * cur.cost;
				continue;
			}

			for(int k = 0; k < S.InDegree[i]; k++)
			{
				int j = S.Pred[i][k].To;
				double d = cur.d + S.Pred[i][k].Length;
				double p = cur.p * S.Prob[j][i];

				if(d <= maxTime)
				{
					Contrib[t].push_back({cur.id, nLabels[t]});
					LabelO2N[t].push_back(nLabels[t]);
					Heap.push_back((HEAPITEM) {d, p, 0., 0., j, nLabels[t]++});
					std::push_heap(Heap.begin(), Heap.end());
				}
			}
		}
	}
}


double GRAPH::GetExactCost(int t, double x)
{
	int nIntervals = Time[t].size();
	int u = 0;
	while(x >= Time[t][u]) u++; //Time[t][...] has a sentinel INFTY at the end

	if(u == nIntervals - 1)
	{
		return LastCost[t];
	}
	else
	{
		return Cost[t][u] + (x * -Slope[t]) + LastCost[t];
	}
}

double PHI(double x, double s)
{
	double xs = x * s;
	return exp(-xs * xs);
}

void GRAPH::GetCostAndDer(int t, double x, double sigma, double& cost, double& der)
{
	struct PHIEVAL
	{
		double sigma;
		double s;
		double erfFactor = .5;
		double phiFactor;

		PHIEVAL(double sigma): sigma(sigma), s(1. / (sigma * sqrt(2))), phiFactor(s / sqrt(M_PI)) {}
		double GetIntegral(double x) {return erf(x * s) * erfFactor;}
		double GetValue(double x) {return PHI(x, s) * phiFactor;}
	};

	PHIEVAL PhiEval(sigma);

	int nIntervals = Time[t].size();
	if(sigma == 0.)
	{
		int u = 0;
		while(x >= Time[t][u]) u++; //Time[t][...] has a sentinel INFTY at the end

		if(u == nIntervals - 1)
		{
			cost = 0.;
			der = 0.;
		}
		else
		{
			cost = Cost[t][u] + (x * -Slope[t]);
			der = -Slope[t];
		}

		return;
	}

	if(nIntervals == 1)
	{
		cost = 0.;
		der = 0.;
		return;
	}

	double x1, x2;
	cost = 0.;
	der = 0.;

	//the first interval
	x2 = x - Time[t][0];
	cost += Cost[t][0] * (.5 - PhiEval.GetIntegral(x2));
	der += Cost[t][0] * (0. - PhiEval.GetValue(x2));

	//the last interval
	x1 = x - Time[t][nIntervals - 2];
	cost += Cost[t][nIntervals - 1] * (PhiEval.GetIntegral(x1) - -.5);
	der += Cost[t][nIntervals - 1] * (PhiEval.GetValue(x1) /* - 0.*/);

	for(int i = 1; i < nIntervals - 1; i++)
	//the intermediate intervals
	{
		x1 = x - Time[t][i - 1];
		x2 = x - Time[t][i];
		cost += Cost[t][i] * (PhiEval.GetIntegral(x1) - PhiEval.GetIntegral(x2));
		der += Cost[t][i] * (PhiEval.GetValue(x1) - PhiEval.GetValue(x2));
	}


	//affine stuff
	x1 = MaxTime[t] - x;
	double erfTerm = -Slope[t] * (PhiEval.GetIntegral(x1) - -.5);
	double phiTerm = -Slope[t] * (PhiEval.GetValue(x1) /* - 0.*/);

	cost += x * erfTerm;
	cost -= sigma * sigma * phiTerm;

	der -= (x + x1) * phiTerm;
	der += erfTerm;

	/*if(der < -1.) der = -1.;
	if(der > 1.) der = 1.;*/

	int u = 0;
    while(x >= Time[t][u]) u++; //Time[t][...] has a sentinel INFTY at the end

    if(u == nIntervals - 1)
    {
        cost = 0.;
    }
    else
    {
        cost = Cost[t][u] + (x * -Slope[t]);
    }
}


void GRAPH::ComputeGradient(int t)
{
	int nEdges = IndexToEdge.size();
	TotalPDForEdge.clear();
	TotalPDForEdge.resize(nEdges);

	size_t s = HeapList[t].size();
	PD.clear();
	PD.resize(s); //zeroed

	for(size_t c = 0; c < s; c++)
	{
		HEAPITEM cur = HeapList[t][c];
		int i = cur.i;

		if((cur.id >= MemorySize[t]) && (Q2V[i] == t))
		//the first condition excludes reaching the target in 0 steps
		{
			PD[c] = ImpArray[i] * cur.cost /* * 1.*/;
		}
	}

	for(auto it = Contrib[t].rbegin(); it != Contrib[t].rend(); it++)
	{
		auto& contrib = *it;

		int r = LabelO2N[t][contrib.first];
		int s = LabelO2N[t][contrib.second];

		int j = HeapList[t][r].i; //search source, edge dest
		int i = HeapList[t][s].i; //search dest, edge source

	//applying chain rule
		PD[r] += PD[s] * S.Prob[i][j];
		TotalPDForEdge[EdgeToIndex[i][j]] += PD[s] * HeapList[t][r].p;
	}
}


void Minimize(double& a, double b)
//assigns b to a provided the new value is less
{
	if(b < a)
	{
		a = b;
	}
}


void Maximize(double& a, double b)
//assigns b to a provided the new value is greater
{
	if(b > a)
	{
		a = b;
	}
}


void
GRAPH::Init(const std::vector<int>& memory,
            const std::vector<std::vector<double>>& time,
            const std::vector<std::vector<double>>& cost,
            const std::vector<EDGE_TYPE>& edge)
{
	n = ParManager.n;
	q = ParManager.q;
	g = ParManager.g;
	qg = ParManager.qg;

	for(int i = 0; i < n; i++)
	{
		MemorySize[i] = memory[i];
	}

	for(int i = 0; i < g; i++)
	{
		if(time[i].size() + 2 != cost[i].size())
		{
			THROW("Unmatching size of input vectors for target " << i \
			    << ": costs should be larger than times by 2, but got costs with size " \
			    << cost[i].size() << " and times with size " << time[i].size() << "!");
		}

		Time[i] = time[i]; //assignment of vectors

		if(Time[i].size())
		{
			MaxTime[i] = Time[i].back();
		}
		else
		{
			MaxTime[i] = -INFTY;
		}

		Time[i].push_back(INFTY); //sentinel value

		Cost[i] = cost[i]; //assignment of vectors



		Slope[i] = Cost[i].back();
		Cost[i].pop_back();
		Cost[i].back() -= Slope[i] * MaxTime[i];
		LastCost[i] = Cost[i].back();

		for(double& c: Cost[i])
		{
			c -= LastCost[i];
		}
	}

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			VEdgeLength[i][j] = INFTY;
		}
	}

	for(auto& [from, to, length]: edge)
	{
		Minimize(VEdgeLength[from][to], length);
	}

//input done

	/*MaxWeight = 0.;

	for(int t = 0; t < g; t++)
	{
		if(Weight[t] > MaxWeight)
		{
			MaxWeight = Weight[t];
			tMaxWeight = t;
		}
	}

	assert(MaxWeight > 0.);*/

	InitVSucc(1);
	FloydWarshall(1e-6);

	for(int i = 0; i < n; i++)
	{
		if(VDist[i][i] < 0.)
		{
			THROW("Vertex " << i << " lies in a cycle of zero length!");
		}
	}

	/*for(int t = 0; t < g; t++)
	//the targets' values are normalized
	//so that the maximum value is 1.
	{
		Weight[t] /= MaxWeight;
	}

	OrigMaxWeight = MaxWeight;
	MaxWeight = 1.;*/

	InitFM();
}

} //namespace PERIODIC_MAINTENANCE
