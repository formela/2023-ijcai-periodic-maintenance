#ifndef UTILS_H
#define UTILS_H

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <vector>
#include <algorithm>
#include <tuple>
#include <sstream>

#define DEFAULT_PATH_MERGE_THRESHOLD 0
//paths whose lengths differ by at most this value are considered equally long;
//if you set this to a negative value, then no merging will happen at all,
//leading to exponentially many paths examined even for graphs with unit edges

#define THROW(s) {std::stringstream str; str << s; throw std::runtime_error(str.str());}

typedef std::tuple<int, int, double> EDGE_TYPE;




#define MAXN 0 //just an identifier - do not change!
#define MAXQ 1 //just an identifier - do not change!
#define MAXG 2 //just an identifier - do not change!
#define MAXQG 3 //just an identifier - do not change!


struct PAR_MANAGER
{
	union
	{
		int par[4];
		struct
		{
			int n, q, g, qg;
		};
	};

	void* pBegin = NULL;
	void* pEnd;

	void Init(void* p, int nb, const std::vector<int>& memory, int g)
	//from GRAPH
	{
		if(pBegin)
		{
			THROW("Trying to create two instances of GRAPH in parallel!");
		}

		pBegin = p;
		pEnd = ((char*) p) + nb;
		n = memory.size();
		this->g = g;

		q = 0;
		for(int i = 0; i < n; i++)
		{
			q += memory[i];
		}

		qg = 0;
		for(int i = 0; i < g; i++)
		{
			qg += memory[i];
		}
	}

	void Init(void* p, int nb, int n)
	//from SCCs
	{
		if(pBegin)
		{
			THROW("Trying to create two instances of GRAPH in parallel!");
		}

		pBegin = p;
		pEnd = ((char*) p) + nb;
		this->n = n;
	}

	void InitFinished()
	{
		pBegin = NULL;
	}
}
ParManager;

template<typename T, int S>
struct ARRAY1D
{
	int size = 0;
	T* a = NULL;

	ARRAY1D()
	{
		if(!((this >= ParManager.pBegin) && (this < ParManager.pEnd)))
		{
			THROW("Trying to create an array outside the current instance of GRAPH!");
		}
		
		size = ParManager.par[S];
		a = new T[size]();
	}

	~ARRAY1D()
	{
		delete[] a;
	}

	ARRAY1D& operator = (ARRAY1D& other)
	{
		for(int i = 0; i < size; i++)
		{
			(*this)[i] = other[i];
		}

		return *this;
	}

	T& operator [] (int i)
	{
		return a[i];
	}
};

template<typename T, int S1, int S2>
struct ARRAY2D
{
	int size1 = 0;
	int size2 = 0;
	T* a = NULL;

	ARRAY2D()
	{
		if(!((this >= ParManager.pBegin) && (this < ParManager.pEnd)))
		{
			THROW("Trying to create an array outside the current instance of GRAPH!");
		}
		
		size1 = ParManager.par[S1];
		size2 = ParManager.par[S2];
		a = new T[size1 * size2];
	}

	~ARRAY2D()
	{
		delete[] a;
	}

	ARRAY2D& operator = (ARRAY2D& other)
	{
		for(int i = 0; i < size1; i++)
		{
			for(int j = 0; j < size2; j++)
			{
				(*this)[i][j] = other[i][j];
			}
		}

		return *this;
	}

	T* operator [] (int i)
	{
		return a + (i * size2);
	}
};

#endif //UTILS_H
