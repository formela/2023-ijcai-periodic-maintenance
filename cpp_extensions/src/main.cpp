#include <torch/extension.h>
#include <ATen/ATen.h>

#include "periodic_maintenance.h"

using namespace pybind11::literals;

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
    m.doc() = "Plugin for fast strategy evaluation";

    py::class_<PERIODIC_MAINTENANCE::GRAPH>(m, "CppPeriodicMaintenanceEvaluator")
        .def(py::init<std::vector<int>,
                std::vector<std::vector<double>>,
                std::vector<std::vector<double>>,
                std::vector<EDGE_TYPE>>(),
            "Initializes the parameters",
            "memory"_a,
            "delimiters"_a,
            "rewards"_a,
            "edges"_a)
        .def("forward",
            &PERIODIC_MAINTENANCE::GRAPH::PyEvaluateStrategy,
            "Evaluates a given strategy tensor.\n"
            "Returns ...",
            "strategy"_a,
            "smoothing"_a)
        .def("backward",
            &PERIODIC_MAINTENANCE::GRAPH::PyComputeGradient,
            "Given an incoming grad returns the strategy_grad.",
            "grad"_a)
        .def("determinize",
            &PERIODIC_MAINTENANCE::GRAPH::PyDeterminize,
            "Determinizes a given randomzied strategy. Returns a pair of vectors: "
            "The first one describes the resulting deterministic strategy - it has length at most max_schedule_length "
            "and contains pairs (vertex, travel time to the next vertex). The second one has length sample_length "
            "and contains, for each sample length, the value of the temporarily best found deterministic schedule.",
            "strategy"_a,
            "sample_length"_a,
            "max_schedule_length"_a,
            "seed"_a, // 0..none, positivi integers..seed
            "init_state"_a=0
            );
}
