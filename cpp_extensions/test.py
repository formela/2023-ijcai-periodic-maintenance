import torch
from evaluators import PeriodicMaintenanceRewards


class Strategy(torch.nn.Module):
    def __init__(self, mask):
        super().__init__()
        self.mask = mask

    def forward(self, strategy_par):
        strategy = torch.exp(strategy_par) * self.mask
        strategy = torch.nn.functional.normalize(strategy, p=1, dim=1)
        return strategy


class Test_PeriodicMaintenance(torch.nn.Module):
    def __init__(self, mask, init_data, smoothing):
        super().__init__()
        self.get_strategy = Strategy(mask)
        self.get_rewards = PeriodicMaintenanceRewards(init_data)
        self.smoothing = smoothing

    def forward(self, strategy_par):
        strategy = self.get_strategy(strategy_par)
        return self.get_rewards(strategy, self.smoothing)


def get_random_strategy(mask, seed=None):
    if seed is not None:
        torch.manual_seed(seed)

    param = torch.rand_like(mask, dtype=torch.float64) * mask
    strategy = torch.nn.functional.normalize(param, p=1, dim=1)
    return strategy


def periodic_maintenance():
    # data = dict(memory=[1, 1],
    #             delimiters=[[1.0, 2.0, 3.0], [1.0]],
    #             rewards=[[1.0, 5.0, 1.0, -10.0], [0.0, 0.0]],
    #             edges=[(0, 1, 1.0),
    #                    (1, 0, 1.0),
    #                    (1, 1, 1.0)])
    data = dict(memory=[1, 1, 1],
                delimiters=[[3, 5], [3, 5], [3, 5]],
                rewards=[[0, 1, -1, 0], [0, 1, -1, 0], [0, 1, -1, 0]],
                edges=[(0, 1, 2),
                       (0, 2, 2),
                       (1, 2, 2),
                       (1, 0, 2),
                       (2, 1, 2),
                       (2, 0, 2)]
                )

    smoothing = 1.0
    get_rewards = PeriodicMaintenanceRewards(data)

    # mask = torch.tensor([[0, 1],
    #                      [1, 1]], dtype=torch.bool)
    mask = torch.tensor([[0, 1, 1],
                         [1, 0, 1],
                         [1, 1, 0]], dtype=torch.bool)

    strategy = get_random_strategy(mask, 0)
    # Define your own fixed valid strategy, if needed
    # strategy = torch.tensor([[0.3, 0.7],
    #                          [0.6, 0.4]], dtype=torch.float64)
    strategy = torch.nn.Parameter(strategy, requires_grad=True)
    print(f'{strategy=}')

    rewards = get_rewards(strategy, smoothing)
    print(f'{rewards=}')

    loss = -torch.sum(torch.flatten(rewards))
    loss.backward()

    print(f'{strategy.grad=}')

    # Gradient Check
    torch.autograd.gradcheck(Test_PeriodicMaintenance(mask, data, smoothing),
                             torch.log(strategy).double(),
                             atol=1e-5, rtol=0, raise_exception=True)


if __name__ == '__main__':
    periodic_maintenance()
