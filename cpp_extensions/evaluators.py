import torch

# # Uncomment for JIT compilation
# from torch.utils.cpp_extension import load
#
# cpp_evaluators = load(name="cpp_evaluators",
#                  sources=["src/main.cpp"],
#                  verbose=True)

from cpp_evaluators import CppPeriodicMaintenanceEvaluator


class PeriodicMaintenanceRewards(torch.nn.Module):
    def __init__(self, init_data):
        super().__init__()
        self.evaluator = CppPeriodicMaintenanceEvaluator(**init_data)

    def forward(self, strategy, smoothing=0):
        strategy = strategy
        evaluator_params = {
            'smoothing': smoothing
        }
        return EvaluatorWrapper.apply(strategy, self.evaluator, evaluator_params)


class EvaluatorWrapper(torch.autograd.Function):
    @staticmethod
    def forward(ctx, strategy, evaluator, evaluator_params):
        prob = evaluator.forward(strategy.detach(), **evaluator_params)
        ctx.evaluator = evaluator
        return prob

    @staticmethod
    def backward(ctx, grad):
        strategy_grad = ctx.evaluator.backward(grad)
        return strategy_grad, None, None
