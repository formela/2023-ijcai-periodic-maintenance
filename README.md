# Mean Payoff Optimization for Systems of Periodic Service and Maintenance (IJCAI 2023)

Consider oriented graph nodes requiring periodic visits by a service agent. The agent moves among the nodes and receives a payoff for each completed service task, depending on the time elapsed since the previous visit to a node. We consider the problem of finding a suitable schedule for the agent to maximize its long-run average payoff per time unit. We propose randomized finite-memory (RFM) schedules as a compact description of the agent's strategies.

Here, we implement an efficient algorithm for constructing RFM schedules. Furthermore, we construct deterministic periodic schedules by sampling from RFM schedules.

To appear in [IJCAI 2023](https://ijcai-23.org/).

David Klaška, Antonín Kučera, Vít Musil, Vojtěch Řehák. Mean Payoff Optimization for Systems of Periodic Service and Maintenance. In Proceedings of the Thirty-Second International Joint Conference on Artificial Intelligence Main Track. Pages 5386-5393. https://doi.org/10.24963/ijcai.2023/598

The arXiv version is available [here](https://arxiv.org/abs/2305.08555).

Up-to-date version of Regstar is at [gitlab.fi.muni.cz/formela/regstar](https://gitlab.fi.muni.cz/formela/regstar/).


## Preparation

Create virtual environment and install all packages:

1. Make sure you have `python 3` and `pipenv` installed. For the up-to-date version, run
```shell
python3 -m pip install --upgrade pip
python3 -m pip install pipenv
cd /the-main-directory-of-implementation/
python3 -m pipenv --rm
```
2. Run `python3 -m pipenv install` (at your own risk with `--skip-lock`)
3. Run the environment `python3 -m pipenv shell`
4. Install ccp_extensions by `cd cpp_extensions; python setup.py build install; cd ..`


## Optimization

Run the optimization (in the above installed pipenv shell) by
```shell
python3 train.py experiments/periodic_maintenance/random_graphs.yml
```
where `experiments/periodic_maintenance/random_graphs.yml` is the path to your configuration file.

For more details, see `setting_template.yml` or the examples in `experiments/.../*.yml`.

## Results

The results can be saved locally to `disk`.

### Disk

Enabled by setting
```yaml
reporting_params:
  database: disk
  database_params:
    results_dir: path/to/a/directory # usually 'results/<experiment_name>/<date>/<time>'
    # more params ...
```
The results, visualizations and logs are stored provided `results_dir`, including

* `stats_epoch.csv` Detailed statistics for all epochs
* `stats_final.csv` Statistics summary over all epochs
* `stats_full.csv` Detailed statistics for all steps of all epochs
* `times_full.csv` Time statistics for all steps of all epochs

and optionally also:

* `check_points` Directory with exports of all torch parameter values
* `strategies` Directory with optimized strategies exported as numpy matrices
* `plots` Directory with plots:
  * `avg_times.pdf` Plot of average runtimes ("forward", "backward", and "other" times)
  * `training_progress.pdf` Plots of strategy values during optimization

