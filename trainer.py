from copy import deepcopy
import numpy as np
import torch
from torch.optim import Adam
from time import process_time
from termcolor import cprint
from utils import get_object


class StrategyTrainer(object):
    def __init__(self, model, report, steps, eval_every=1,
                 optimizer_params=None,
                 pre_train_hooks=None, post_train_hooks=None,
                 pre_step_hooks=None, post_step_hooks=None,
                 verbose=False):
        optimizer_params = optimizer_params or {}
        self.pre_train_hooks = pre_train_hooks or {}
        self.post_train_hooks = post_train_hooks or {}
        self.pre_step_hooks = pre_step_hooks or {}
        self.post_step_hooks = post_step_hooks or {}
        self.verbose = verbose
        self.report = report
        self.step_num = 0
        self.val = 0.0
        self.strategy = None
        self.state_dict = None
        self.val_train = None
        self.loss = None
        self.times = None
        self.extras = None
        self.eval_extras = {}
        self.steps = steps
        self.eval_every = eval_every
        self.model = model
        self.optimizer = Adam(params=self.model.parameters(), **optimizer_params)

    def eval(self):
        self.model.eval()
        with torch.no_grad():
            if self.val_train is not None:
                self.val = self.val_train
                self.strategy = self.model.strategy().detach().numpy()
            else:
                self.val, self.strategy, self.eval_extras = self.model()
            self.state_dict = deepcopy(self.model.state_dict())
            self.print_eval()

    def step(self):
        start_time = process_time()
        self.model.train()
        self.print_start_step()
        self.optimizer.zero_grad()
        self.model.schedule(self.steps, self.step_num)
        self.loss, self.val_train, self.extras = self.model()
        self.print_step()
        forward_time = process_time()
        self.loss.backward()
        self.print_grad()
        backward_time = process_time()
        self.optimizer.step()
        optimizer_time = process_time()
        if self.step_num % self.eval_every == 0 or self.step_num == 1:
            self.eval()
        eval_time = process_time()

        self.times = dict(
            f_time=forward_time - start_time,
            b_time=backward_time - forward_time,
            o_time=optimizer_time - backward_time,
            e_time=eval_time - optimizer_time)
        self.log()

    def train(self):
        for hook, hook_params in self.pre_train_hooks.items():
            get_object(hook)(self.model, self.report.writer, **hook_params)

        while self.step_num < self.steps:
            self.step_num += 1
            for hook, hook_params in self.pre_step_hooks.items():
                get_object(hook)(self.model, self.report.writer, self.step_num, **hook_params)

            self.step()

            for hook, hook_params in self.post_step_hooks.items():
                get_object(hook)(self.model, self.report.writer, self.step_num, **hook_params)

        if self.steps == 0:
            start_time = process_time()
            self.eval()
            eval_time = process_time()
            self.times = dict(e_time=eval_time - start_time)
            self.log()

        for hook, hook_params in self.post_train_hooks.items():
            get_object(hook)(self.model, self.report.writer, **hook_params)

    def log(self):
        self.extras = self.extras or {}
        self.eval_extras = self.eval_extras or {}
        self.report.update(
            step_num=self.step_num,
            val=self.val,
            val_train=self.val_train,
            loss=self.loss.item() if self.loss else self.loss,
            strategy=self.strategy,
            state_dict=self.state_dict,
            extras={**self.extras, **self.eval_extras},
            **self.times)

    def print_eval(self):
        if self.verbose:
            cprint(f'step={self.step_num} (eval)\t val={self.val:.2f}', 'cyan')
            self.model.graph.print_strategy(self.strategy, style='matrix', precision=3)

    def print_start_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train) started', 'green')

    def print_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train)\t val={self.val_train:.2f}\t loss={self.loss:.2f}', 'green')

    def print_grad(self):
        if self.verbose:
            with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
                cprint(f'Strategy grad:', 'blue')
                print(' ', np.array2string(self.model.strategy.params.grad.numpy(), prefix='  '))