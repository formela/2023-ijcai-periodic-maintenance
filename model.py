import torch
from termcolor import cprint
from cpp_extensions.evaluators import PeriodicMaintenanceRewards


class Strategy(torch.nn.Module):
    def __init__(self, mask, rounding_threshold):
        super().__init__()
        self.mask = mask
        self.threshold = rounding_threshold
        self.params = torch.nn.Parameter(self._init_params())

    def _init_params(self):
        return self.mask * torch.log(torch.rand_like(self.mask, dtype=torch.float64))

    def reset_parameters(self):
        self.params.data = self._init_params()

    def forward(self):
        x = torch.exp(self.params) * self.mask
        strategy = torch.nn.functional.normalize(x, p=1, dim=-1)
        if not self.training:
            x = torch.threshold(strategy, threshold=self.threshold, value=0.0)
            strategy = torch.nn.functional.normalize(x, p=1, dim=-1)
        return strategy


class Model(torch.nn.Module):
    def __init__(self, graph, strategy_params, objective_params=None):
        super().__init__()
        objective_params = objective_params or {}
        self.graph = graph
        self.strategy = Strategy(graph.mask, **strategy_params)
        self.objective = PeriodicMaintenanceObjective(graph, **objective_params)


    def schedule(self, *args, **kwargs):
        self.objective.schedule(*args, *kwargs)

    def forward(self):
        return self.objective(self.strategy())


class PeriodicMaintenanceObjective(torch.nn.Module):
    mode = 'max'

    def __init__(self, graph,
                 smoothing,
                 smoothing_end,
                 verbose=False):
        torch.nn.Module.__init__(self)
        self.graph = graph
        self.mask = graph.mask
        self.times = graph.times
        self.aug_nodes_n = graph.aug_nodes_n
        self.verbose = verbose
        self.smoothing = smoothing
        self.smoothing_end = smoothing_end
        self._current_smoothing = smoothing
        self.rewards = PeriodicMaintenanceRewards(graph.get_maintenance_init_data())

    def get_node_stationary(self, strategy):
        B = strategy.transpose(0, 1) - torch.eye(self.aug_nodes_n)
        A = torch.cat((B[:-1], torch.ones_like(B[0])[None, :]), 0)
        b = torch.zeros_like(strategy[0])
        b[-1] = 1
        return solve_system(A, b, self.verbose)

    def get_edge_stationary(self, strategy):
        node_stationary = self.get_node_stationary(strategy)
        distribution = strategy * node_stationary[:, None] * self.times
        return distribution / distribution.sum()

    def determinize(self, strategy, sample_length, max_schedule_length, init_state=0, seed=None):
        seed = 0 if seed is None else seed + 1
        return self.rewards.evaluator.determinize(
            strategy=strategy,
            sample_length=sample_length,
            max_schedule_length=max_schedule_length,
            seed=seed,
            init_state=init_state
        )

    def get_time_weighted_node_dist(self, strategy):
        node_stationary = self.get_node_stationary(strategy)
        distribution = strategy * node_stationary[:, None] * (self.times)
        return node_stationary / distribution.sum()

    def schedule(self, total_steps, current_step):
        self._current_smoothing = self.smoothing * max(0, 1 - current_step / (self.smoothing_end * total_steps))

    def forward(self, strategy):
        distr = self.get_time_weighted_node_dist(strategy)
        rewards = self.rewards(strategy, self._current_smoothing)
        val = torch.sum(rewards * distr) + self.graph.reward_decay

        if self.training:
            extras = {
                'smoothing': self._current_smoothing,
            }
            return -val, val.item(), extras
        else:
            return val.item(), strategy.detach().numpy(), None


def solve_system(A, b, verbose=False):
    try:
        solution = torch.linalg.solve(A, b)
    except RuntimeError as error_msg:
        cprint('torch.linalg.solve failed with runtime error:', 'red')
        print(error_msg)
        if verbose:
            cprint('A:', 'blue')
            print(A)
            cprint('b:', 'blue')
            print(b)
        return None
    return solution


class RoundDown(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, base):
        if base == 0:
            return x
        else:
            return base * torch.floor(x / base)

    @staticmethod
    def backward(ctx, x_grad):
        return x_grad, None
