from collections import Counter
import torch
import networkx
import numpy as np
from itertools import product
from termcolor import cprint
from graph_collections import square_subgraph_periodic_maintenance

class Graph(object):
    def __init__(self, loader, loader_params, modifiers=None, verbose=False, name=None, global_memory=1):
        self.verbose = verbose
        self.name = name
        self.global_memory = global_memory

        self.graph = square_subgraph_periodic_maintenance(**loader_params)
        self.graph = self.graph.to_directed()

        self.targets, self.non_targets = [], []
        for (node, is_target) in sorted(self.graph.nodes.data('target')):
            (self.non_targets, self.targets)[is_target].append(node)
        self.targets_n = len(self.targets)

        self.nodes = self.targets + self.non_targets  # external evaluators require targets first
        self.nodes_n = len(self.nodes)

        # collecting node features
        target_features = ['value', 'memory', 'attack_len', 'blindness', 'delimiters', 'rewards']
        self.target_features = get_node_features(self.graph, self.targets, target_features)
        non_target_features = ['memory']
        self.non_target_features = get_node_features(self.graph, self.non_targets, non_target_features)

        # integer encoding for external evaluators
        self.node_map = {node: index for index, node in enumerate(self.nodes)}
        self.edges = [(self.node_map[a], self.node_map[b], att['len']) for a, b, att in self.graph.edges.data()]

        # graph augmentation with memory elements
        self.aug_nodes, aug_nodes_att = [], []
        for node in self.nodes:
            self.aug_nodes += [(node, mem) for mem in range(self.graph.nodes[node]['memory'])]
        self.aug_node_map = {node: index for index, node in enumerate(self.aug_nodes)}
        self.aug_nodes_n = len(self.aug_nodes)

        self.aug_graph = networkx.DiGraph()
        self.aug_graph.add_nodes_from(self.aug_nodes)  # the nodes do not contain their attributes

        for source, destination, att in self.graph.edges.data():
            source_memory = self.graph.nodes[source]['memory']
            destination_memory = self.graph.nodes[destination]['memory']
            mem_product = product(range(source_memory), range(destination_memory))
            aug_edges = [((source, ms), (destination, mt), att) for ms, mt in mem_product]
            self.aug_graph.add_edges_from(aug_edges)

        # edge_len matrix indexed by aug_nodes
        times = networkx.to_numpy_array(self.aug_graph, nodelist=self.aug_nodes, weight='len')
        self.times = torch.from_numpy(times)

        # binary matrix; edge encodings of aug_edges
        self.mask = self.times > 0

        rewards = self.target_features['rewards']
        delimiters = self.target_features['delimiters']
        nan_rewards = [type(x) is float and np.isnan(x) for x in rewards]
        if not(all(nan_rewards)):
            # there are some rewards
            for rew, deli in zip(rewards, delimiters):
                if isinstance(rew, list):
                    rew += [0] * (2 + len(deli) - len(rew))
            self.reward_decay = torch.tensor(sum([reward[-1] for reward in self.target_features['rewards']]))

        if verbose:
            self.print_graph_stats()

    def get_maintenance_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'delimiters': self.target_features['delimiters'],
                'rewards': self.target_features['rewards'],
                'edges': self.edges}
        return data

    def __repr__(self):
        return self.name or self.graph.name or self.__class__.__name__

    def print_strategy(self, strategy, style='label', precision=3, print_len=False):
        cprint('Strategy:', 'blue')
        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(' ', np.array2string(strategy, prefix='  '))
                if print_len:
                    cprint('Length:', 'blue')
                    print(' ', np.array2string(self.times.numpy(), prefix='  '))
        else:
            print('    src--dest :\tprob' + ' ' * (precision - 1) + '\t' + 'length' * print_len)
            for (source, ms), (destination, mt), length in self.aug_graph.edges(data='len'):
                source_index = self.aug_node_map[(source, ms)]
                destination_index = self.aug_node_map[(destination, mt)]
                prob = strategy[source_index, destination_index]
                if prob > 0:
                    if style == 'label':
                        edge_str = f'{source} ({ms})--{destination} ({mt})'
                    elif style == 'index':
                        edge_str = f'{source_index:5.0f}--{destination_index:3.0f}  '
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')
                    print(f'  {edge_str}:\t{prob:.{precision}f}\t'
                          + f' {length:.{precision}f}' * print_len)


def get_node_features(graph, nodes, features):
    return {key: [graph.nodes[n].get(key, float('nan')) for n in nodes] for key in features}
