from graph_utils import Graph
from reporting import ExperimentReporting, RunReporting
from model import PeriodicMaintenanceObjective, Model
from trainer import StrategyTrainer
from utils import set_seed, parse_params


@parse_params()
def main(seed, verbosity, reporting_params, epochs, graph_params, model_params, trainer_params):
    settings = locals()
    mode = PeriodicMaintenanceObjective.mode
    report = ExperimentReporting(**reporting_params, settings=settings, mode=mode, verbose=(verbosity >= 1))

    graph = Graph(**graph_params, verbose=(verbosity >= 2))

    for epoch in range(epochs):
        set_seed(seed, epoch)
        model = Model(graph, **model_params)
        run_report = RunReporting(epoch, **report.params)
        StrategyTrainer(model, run_report, **trainer_params, verbose=(verbosity >= 3)).train()
        metrics = run_report.dump()
        report.update(metrics)

    report.dump()


if __name__ == '__main__':
    main()
